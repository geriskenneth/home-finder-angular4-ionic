import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {ProfilePage} from '../profile/profile';

@IonicPage()
@Component({
    selector: 'page-search',
    templateUrl: 'search.html',
})
export class SearchPage {

    constructor(public navCtrl: NavController) {}
    goToProfile() {
        this.navCtrl.push(ProfilePage);
    }
}
