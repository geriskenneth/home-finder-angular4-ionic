import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-model-detail',
  templateUrl: 'model-detail.html',
})
export class ModelDetailPage {

  constructor(public navCtrl: NavController) {
  }
}
