import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModelDetailPage } from './model-detail';

@NgModule({
  declarations: [
    ModelDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ModelDetailPage),
  ],
})
export class ModelDetailPageModule {}
