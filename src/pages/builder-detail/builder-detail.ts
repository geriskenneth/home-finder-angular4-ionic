import {Component} from '@angular/core';
import {NavController, NavParams,IonicPage} from 'ionic-angular';
import {EventData} from '../../providers/event-data';

@IonicPage()
@Component({
    selector: 'page-builder-detail',
    templateUrl: 'builder-detail.html',
})
export class BuilderDetailPage {
    currentBuilder: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public eventData: EventData) {
        this.eventData.getBuilderDetail(this.navParams.get('builderId')).subscribe(builderDetail => {
            this.currentBuilder = builderDetail;
        });
    }
}
