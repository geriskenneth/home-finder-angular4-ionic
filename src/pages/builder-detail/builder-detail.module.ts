import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuilderDetailPage } from './builder-detail';

@NgModule({
  declarations: [
    BuilderDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BuilderDetailPage),
  ],
})
export class BuilderDetailPageModule {}
