import {Component} from '@angular/core';
import {NavController, LoadingController, AlertController, IonicPage} from 'ionic-angular';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthData} from '../../providers/auth-data';
import {EmailValidator} from '../../validators/email';
import {HomePage} from '../home/home';

@IonicPage()
@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html',
})
export class SignupPage {
    public signupForm;
    emailChanged: boolean = false;
    passwordChanged: boolean = false;
    submitAttempt: boolean = false;
    loading: any;
    constructor(public navCtrl: NavController, public authData: AuthData, public formBuilder: FormBuilder,
        public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

        this.signupForm = formBuilder.group({
            email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
            password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
        })
    }
    elementChanged(input) {
        let field = input.inputControl.name;
        this[field + "Changed"] = true;
    }
    signupUser() {
        this.submitAttempt = true;
        if (!this.signupForm.valid) {
        } else {
            this.authData.signupUser(this.signupForm.value.email, this.signupForm.value.password).then(() => {
                this.navCtrl.setRoot(HomePage);
            }, (error) => {
                this.loading.dismiss();
                let alert = this.alertCtrl.create({
                    message: error.message,
                    buttons: [
                        {
                            text: "Ok",
                            role: 'cancel'
                        }
                    ]
                });
                alert.present();
            });

            this.loading = this.loadingCtrl.create({
                dismissOnPageChange: true,
            });
            this.loading.present();
        }
    }
}
