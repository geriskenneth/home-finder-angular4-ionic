import {Component} from '@angular/core';
import {NavController, NavParams, ToastController, ModalController} from 'ionic-angular';
import {ProfilePage} from '../profile/profile';
import {EventData} from '../../providers/event-data';
import {EventDetailPage} from '../event-detail/event-detail';
import {BuilderDetailPage} from '../builder-detail/builder-detail';
import {CommentPage} from '../comment/comment';


@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage {
    communities: any;
    isOn: boolean = false;
    communityList: any;
    loadedCommunitiesList: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public eventData: EventData, public toastCtrl: ToastController, public modalCtrl: ModalController) {
        this.communities = this.eventData.getCommunitiesList();
    }

    goToProfile() {
        this.navCtrl.push(ProfilePage);
    }
    goToCommunityDetail(communityId) {
        this.navCtrl.push(EventDetailPage, {
            communityId: communityId
        });
    }
    goToBuilder(builderId) {
        this.navCtrl.push(BuilderDetailPage, {
            builderId: builderId
        })
    }

    followCommunity(communityId) {
        this.eventData.followCommunity(communityId);
        let toast = this.toastCtrl.create({
            message: 'Added to following',
            duration: 1000
        });
        toast.present();
    }
    searchCommunities() {
        this.isOn = !this.isOn;
    }
    initializeCommunities(): void {
        this.communityList = this.loadedCommunitiesList;
    }
    getCommunities(ev: any) {
        this.initializeCommunities();
        var q = ev.srcElement.value;

        if (!q) {return;}
        this.communityList = this.communityList.filter((v) => {
            if (v.community || v.builder && q) {
                return (v.community.toLowerCase().indexOf(q.toLowerCase()) > -1 || v.builder.toLowerCase().indexOf(q.toLowerCase()) > -1);
            }
        });
    };

    commentModal(communityId) {
        let modal = this.modalCtrl.create(CommentPage, {communityId: communityId});
        modal.present();
    }

    likeCommunity(communityId) {
        this.eventData.likeCommunity(communityId);
    }

}
