import {Component} from '@angular/core';
import {NavController, IonicPage} from 'ionic-angular';
import {ProfilePage} from '../profile/profile';
import {BuilderDetailPage} from '../builder-detail/builder-detail';
import firebase from 'firebase';

@IonicPage()
@Component({
    selector: 'page-builders',
    templateUrl: 'builders.html',
})
export class BuildersPage {
    isOn: boolean = false;
    builderList: any;
    builderRef: any;
    loadedBuilderList: any;
    constructor(public navCtrl: NavController) {
        this.navCtrl = navCtrl;
        this.builderRef = firebase.database().ref('/Builders');
        this.builderRef.once('value', builderList => {
            let builders = [];
            builderList.forEach(builder => {
                builders.push(builder.val());
            });
            this.builderList = builders;
            this.loadedBuilderList = builders;
        });
    }
    goToProfile() {
        this.navCtrl.push(ProfilePage);
    }
    goToBuilderDetail(builderId) {
        this.navCtrl.push(BuilderDetailPage, {
            builderId: builderId,
        });
    }
    searchBuilders() {
        this.isOn = !this.isOn;
    }
    initializeBuilders(): void {
        this.builderList = this.loadedBuilderList;
    }
    getBuilders(ev: any) {
        this.initializeBuilders();
        var q = ev.srcElement.value;

        if (!q) {return;}
        this.builderList = this.builderList.filter((v) => {
            if (v.builder && q) {
                return (v.builder.toLowerCase().indexOf(q.toLowerCase()) > -1);
            }
        });
    };
}
