import {Component} from '@angular/core';
import {NavController, IonicPage} from 'ionic-angular';
import {ProfilePage} from '../profile/profile';

@IonicPage()
@Component({
    selector: 'page-news',
    templateUrl: 'news.html',
})
export class NewsPage {

    constructor(public navCtrl: NavController) {}
    goToProfile() {
        this.navCtrl.push(ProfilePage);
    }
}
