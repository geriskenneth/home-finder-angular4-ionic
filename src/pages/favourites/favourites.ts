import {Component} from '@angular/core';
import {NavController, NavParams, IonicPage} from 'ionic-angular';
import {ProfilePage} from '../profile/profile';

@IonicPage()
@Component({
    selector: 'page-favourites',
    templateUrl: 'favourites.html',
})
export class FavouritesPage {
    constructor(public navCtrl: NavController, public navParams: NavParams) {}
    goToProfile() {
        this.navCtrl.push(ProfilePage);
    }

}
