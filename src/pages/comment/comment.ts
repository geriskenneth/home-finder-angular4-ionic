import {Component} from '@angular/core';
import {NavController, NavParams, ViewController, IonicPage} from 'ionic-angular';
import {EventData} from '../../providers/event-data';
import {Observable} from 'rxjs/Observable';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';

@IonicPage()
@Component({
    selector: 'page-comment',
    templateUrl: 'comment.html',
})
export class CommentPage {
    public paramsCommunity: any;
    public comments: Observable<any[]>;
    private comment: FormGroup;
    todo = {}
    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public params: NavParams, public eventData: EventData, public formBuilder: FormBuilder) {
        this.paramsCommunity = params.get('communityId');
        this.comments = this.eventData.getCommentsList(this.paramsCommunity);
        this.comment = this.formBuilder.group({
            comments: ['', Validators.required],
        });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
    submitComment() {
        this.eventData.postComment(this.comment.get('comments').value, this.paramsCommunity).then(() => {
            this.comment.reset();
        })
    }

}
