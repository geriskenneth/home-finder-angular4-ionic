import {Component} from '@angular/core';
import {NavController, NavParams, IonicPage} from 'ionic-angular';
import {EventData} from '../../providers/event-data';
import {ModelDetailPage} from '../model-detail/model-detail';

@IonicPage()
@Component({
    selector: 'page-event-detail',
    templateUrl: 'event-detail.html',
})
export class EventDetailPage {
    currentCommunity: any; currentCommunityModels: any;
    info: any;
    modelist = [];
    constructor(public navCtrl: NavController, public navParams: NavParams, public eventData: EventData) {
        this.info = "general";
        this.eventData.getCommunityDetail(this.navParams.get('communityId')).subscribe(communityDetail => {
            this.currentCommunity = communityDetail;
        });
    }
    goToModelDetail(communityId, modelId) {
        this.navCtrl.push(ModelDetailPage, {
            communityId: communityId,
            modelId: modelId,
        });
    }
}
