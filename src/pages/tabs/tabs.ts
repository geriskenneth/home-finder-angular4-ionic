import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {NewsPage} from '../news/news';
import {HomePage} from '../home/home';
import {BuildersPage} from '../builders/builders';
import {FavouritesPage} from '../favourites/favourites';
import {SearchPage} from '../search/search';


@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html'
})
export class TabsPage {
    tab1Root: any = NewsPage;
    tab2Root: any = HomePage;
    tab3Root: any = BuildersPage;
    tab4Root: any = FavouritesPage;
    tab5Root: any = SearchPage;

    constructor(public navCtrl: NavController, public navParams: NavParams) {}

}




