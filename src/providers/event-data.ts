import {Injectable, Inject} from '@angular/core';
import {AngularFireDatabase, } from 'angularfire2/database';
import {FirebaseApp} from 'angularfire2';
import firebase from 'firebase';
import {ProfileData} from '../providers/profile-data';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class EventData {
    communitiesList: Observable<any[]>;
    buildersList: Observable<any[]>;
    public currentUser: any;
    public profilePictureRef: any;
    public userProfile: any;
    public firstName: string; public lastName: string;
    counter: any;
    public mLike: boolean;
    public item: any;

    constructor(public afDB: AngularFireDatabase, @Inject(FirebaseApp) FirebaseApp: any, public profileData: ProfileData) {
        this.currentUser = FirebaseApp.auth().currentUser.uid;
        this.communitiesList = afDB.list('Communities').valueChanges();
        this.buildersList = afDB.list('Builders').valueChanges();

        this.profilePictureRef = FirebaseApp.storage().ref('/guestProfile/');

        this.profileData.getUserProfile().once('value', (data) => {
            this.userProfile = data.val();
            this.firstName = this.userProfile.firstName;
            this.lastName = this.userProfile.lastName;
        });

    }

    getFollowList(): any {
        //    return ''/*this.followList;*/
    }

    getCommunitiesList(): any {
        return this.communitiesList;
    }

    getCommunityDetail(communityId): any {
        return this.afDB.object('Communities/' + communityId).valueChanges();
    }
    getCurrentUser(): any {
        //        return this.currentUser;
    }

    getBuildersList(): any {
        return this.buildersList;
    }
    getModelDetail(communityId, modelId): any {
        return this.afDB.object('Models/' + communityId + '/' + modelId).valueChanges();
    }
    getModelsList(communityId, modelId): any {
        //        return this.af.database.list('Models/'+communityId+'/');
    }

    getBuilderDetail(builderId): any {
        return this.afDB.object('Builders/' + builderId + '/').valueChanges();
    }

    postComment(comment: any, communityId: any): any {
        //        ken.on('value', function(snapshot) {
        //            console.log(snapshot.val());
        //            console.log(firebase.database().ref('Communities/' + communityId + '/comments').transaction(function(post){
        //                
        //            }));
        //        });
        return this.afDB.list('Comments/' + communityId + '/').push({
            comment: comment,
            user: this.firstName + ' ' + this.lastName,
            time: new Date().getTime(),
        }).then(() => {
            firebase.database().ref('Communities/' + communityId + '/comments').transaction(comments => {
                if (comments != null) {
                    comments++;
                } else {
                    comments = 1;
                }
                return comments;
            })
        })
    }

    getCommentsList(communityId: any): any {
        return this.afDB.list('Comments/' + communityId).valueChanges();
    }


    likeCommunity(communityId: any): any {
        this.mLike = true;
        let user = this.currentUser;
        var starCountRef = this.afDB.object('Likes/'+user+'/'+communityId).snapshotChanges();
        starCountRef.subscribe(snapshot => {
            if (this.mLike) {
                if (snapshot.key != null) {
                    this.afDB.object('Likes/'+user+'/'+communityId).remove();
                    firebase.database().ref('Communities/' + communityId + '/likes').transaction(likes => {
                        if (likes === null) return likes = 0;
                        else return likes - 1;
                    })
                    this.mLike = false;
                }
                else {
                    this.afDB.object('Likes/'+user+'/'+communityId).set(true);
                    firebase.database().ref('Communities/' + communityId + '/likes').transaction(likes => {
                        if (likes === null) return likes = 0;
                        else return likes + 1;
                    })
                    this.mLike = false;
                }
            }
        });
    }
    /*unlikeCommunity(communityId: any): any{
        return this.af.database.object('Likes/'+this.currentUser+'/'+communityId).set(false).then(()=>{
            this.af.database.object('Communities/'+communityId+'/likes').$ref.ref.transaction(likes => {
                if (likes === null) {
                    return likes = 0;
                } else {
                    return likes - 1;
                }                
            })
        });
    }    */
    getLikedCommunity() {
        //        return this.af.database.list('Likes/'+this.currentUser);
    }

    followCommunity(communityId: any): any {
        //        this.af.database.object('Following/'+this.currentUser+'/'+communityId).set(true);
    }
    getFollowingCommunity() {
        //        return this.af.database.list('Following/'+this.currentUser);
    }

    unfollowCommunity(communityId: any): any {
        //        this.af.database.object('Following/'+this.currentUser+'/'+communityId).remove();

    }
    /*
    getLikes(userId: any): any{
        return this.af.database.list('userProfile/'+userId+'/likes')
    }*/

    /*
        createEvent(builderName: string, communityName: string, priceRange: number, openingDate: string, communityPicture: any): any {
          return this.eventList.push({
            builder: builderName,
            community: communityName,
            pricerange: priceRange,
            date: openingDate,
          }).then( newEvent => {
            this.eventList.child(newEvent.key).child('id').set(newEvent.key);
            if (communityPicture != null) {
              this.profilePictureRef.child(newEvent.key).child('communityPicture.png')
            .putString(communityPicture, 'base64', {contentType: 'image/png'})
              .then((savedPicture) => {
                this.eventList.child(newEvent.key).child('communityPicture')
                .set(savedPicture.downloadURL);
              });        
            }
          });
        }
    
    /*
        addGuest(guestName, eventId, eventPrice, guestPicture = null): any {
          return this.eventList.child(eventId).child('guestList').push({
            guestName: guestName
          }).then((newGuest) => {
            this.eventList.child(eventId).transaction( (event) => {
              event.revenue += eventPrice;
              return event;
            });
            if (guestPicture != null) {
              this.profilePictureRef.child(newGuest.key).child('profilePicture.png')
            .putString(guestPicture, 'base64', {contentType: 'image/png'})
              .then((savedPicture) => {
                this.eventList.child(eventId).child('guestList').child(newGuest.key).child('profilePicture')
                .set(savedPicture.downloadURL);
              });        
            }
          });
        }
        */
}
