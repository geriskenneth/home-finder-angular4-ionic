import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {MomentModule} from 'angular2-moment';
import { ReactiveFormsModule } from '@angular/forms';


/*PAGES*/
import {MyApp} from './app.component';
import {TabsPage} from '../pages/tabs/tabs';
import {HomePage} from '../pages/home/home';
import {BuildersPage} from '../pages/builders/builders';
import {ProfilePage} from '../pages/profile/profile';
import {FavouritesPage} from '../pages/favourites/favourites';
import {SearchPage} from '../pages/search/search';
import {NewsPage} from '../pages/news/news';
import { BuilderDetailPage } from '../pages/builder-detail/builder-detail';
import { CommentPage } from '../pages/comment/comment';
import { LoginPage } from '../pages/login/login';
import { EventDetailPage } from '../pages/event-detail/event-detail';
import { ModelDetailPage } from '../pages/model-detail/model-detail';
import { SignupPage } from '../pages/signup/signup';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';

/*PROVIDERS*/
import { EventData } from '../providers/event-data';
import { ProfileData } from '../providers/profile-data';
import { AuthData } from '../providers/auth-data';

/*ANGULARFIRE*/
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule, AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';

export const firebaseConfig = {
    apiKey: "AIzaSyASRm9mi-tZ1WhcWtFavWFyfmaGQebE5pQ",
    authDomain: "builders-38a10.firebaseapp.com",
    databaseURL: "https://builders-38a10.firebaseio.com",
    storageBucket: "builders-38a10.appspot.com",
    messagingSenderId: "161967529916"
};


@NgModule({
    declarations: [
        MyApp,
        TabsPage,
        FavouritesPage,
        SearchPage,
        NewsPage,
        HomePage,
        BuildersPage,
        ProfilePage,
        CommentPage,
        BuilderDetailPage,
        LoginPage,
        EventDetailPage,
        ModelDetailPage,
        SignupPage,
        ResetPasswordPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        MomentModule,
        ReactiveFormsModule,
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        TabsPage,
        FavouritesPage,
        SearchPage,
        NewsPage,
        HomePage,
        BuildersPage,
        ProfilePage,
        CommentPage,
        BuilderDetailPage,
        LoginPage,
        EventDetailPage,
        ModelDetailPage,
        SignupPage,
        ResetPasswordPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        AngularFireDatabase,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        EventData,
        ProfileData,
        AuthData
    ]
})
export class AppModule {}
