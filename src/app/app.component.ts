import { Component, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  zone: NgZone;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.zone = new NgZone({});
    firebase.initializeApp({
    apiKey: "AIzaSyASRm9mi-tZ1WhcWtFavWFyfmaGQebE5pQ",
    authDomain: "builders-38a10.firebaseapp.com",
    databaseURL: "https://builders-38a10.firebaseio.com",
    storageBucket: "builders-38a10.appspot.com",
    messagingSenderId: "161967529916"
    });

    firebase.auth().onAuthStateChanged((user) => {
      this.zone.run( () => {
        if (!user) {
          this.rootPage = LoginPage;
        } else { 
          this.rootPage = TabsPage;
        }
      });     
    });

  }
}

